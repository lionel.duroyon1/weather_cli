import subprocess
import json

from is_rainy import EXIT_UNKNOWN_LOCATION

weather_possible_values = ["Snow", "Sleet", "Hail", "Thunderstorm", "Heavy Rain",
                            "Light Rain", "Showers", "Heavy Cloud", "Light Cloud", "Clear"]

def test_some_capitals():
    cities = ["Toronto", "Bangkok", "Paris"]

    for city in cities:
        res = subprocess.check_output(["./is_rainy.py", city])
        # Bytes to string, removing trailing char
        res = res.decode("utf-8").rstrip("\n")
        # Str to dict
        res = json.loads(res)
        # Assert that the values are acceptable
        assert city in res
        assert res[city] in weather_possible_values

def test_no_data():
    city = "uknown_city"
    try:
        res = subprocess.check_output(["./is_rainy.py", city])
    except Exception as err:
        assert f"non-zero exit status {EXIT_UNKNOWN_LOCATION}" in str(err)