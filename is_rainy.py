#! /usr/bin/env python3
"""
Command line interface to get weather for a city givenas argument
Help with args : ./is_rainy.py -h
"""
import argparse
import datetime
import json
import logging
import sys
from datetime import date

import requests

# Url path constants
BASE_URL = "https://www.metaweather.com/api/"
SEARCH_LOCATION_URL = f"{BASE_URL}/location/search/?query="
LOCATION_URL = f"{BASE_URL}/location/"

# Errors code constants
EXIT_UNKNOWN_LOCATION = 3
EXIT_NO_FORECAST_DATA = 2

# Configuring logger
logger = logging.getLogger('city_weather')
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
formatter = \
    logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)


def no_forecast_error():
    """ Error function if no data found """
    logger.info("no forecast data found")
    sys.exit(EXIT_NO_FORECAST_DATA)


def handling_args():
    """ Handle scripts args """
    parser = argparse.ArgumentParser()
    parser.add_argument("--debug",
                        dest="debug",
                        action="store_true",
                        help="activate debug logs")
    parser.add_argument("city", help="city for weather forecast")
    return parser.parse_args()


def configure_log_level(args):
    """ Configuring log level according to debug args """
    if args.debug:
        ch.setLevel(logging.DEBUG)
    else:
        ch.setLevel(logging.INFO)


def get_city_infos(args, location):
    """ Getting info about the city, handle error if city is not found """
    if not json.loads(location.content):
        logger.warning("unknown location : %s", args.city)
        sys.exit(EXIT_UNKNOWN_LOCATION)
    else:
        logger.debug(location.content)
        city = json.loads(location.content)[0]
        logger.debug("location woeid : %s", city["woeid"])
        return city


def get_tomorrow():
    """ Return tomorrow date str, formatted with "%Y-%m-%d" """
    tomorrow = (date.today() + datetime.timedelta(1)).strftime("%Y-%m-%d")
    logger.debug("tomorrow : %s", tomorrow)
    return tomorrow


def searching_tomorrow_forecast(city, consolidated_weather):
    """ Searching for tomorrow forecast
    Args:
        city (dict): city dict contains keys : title, woeid.
        consolidated_weather (dict): consolidated weather dict,
         consolidated_weather key contains forecast_list.
    """
    tomorrow = get_tomorrow()

    # Extracting forecast data from api response
    consolidated_weather_dict = json.loads(consolidated_weather.content)
    if consolidated_weather_dict:
        forecast_list = consolidated_weather_dict["consolidated_weather"]
    else:
        no_forecast_error()

    for forecast in forecast_list:
        if forecast["applicable_date"] == tomorrow:
            # This is what we are looking for
            logger.info("Tomorrow forecast for %s : %s",
                        city["title"],
                        forecast['weather_state_name'])
            print(json.dumps({city["title"]: forecast['weather_state_name']}))
            sys.exit(0)

    no_forecast_error()


def main():
    """ Script main function """
    args = handling_args()

    configure_log_level(args)

    # Location request
    location = requests.get(SEARCH_LOCATION_URL + args.city)

    # Getting info about the city
    city = get_city_infos(args, location)

    # Weather request on the city found
    consolidated_weather = requests.get(LOCATION_URL + str(city["woeid"]))
    logger.debug(consolidated_weather.content)

    # Searching for tomorrow forecast for the city found
    searching_tomorrow_forecast(city, consolidated_weather)


if __name__ == "__main__":
    main()
