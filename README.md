# Weather_cli
This is a Weather command line interface using : https://www.metaweather.com/api/

The purpose of the script is to give tommorow weather forecast for a city given as argument

# Requirements
## Python 3
check that you have a working python3
```python -V```

## External libs
## Please launch the following command in a virtualenv
```pip install -r requirements.txt```

# Usage
## Help on arguments
```./is_rainy.py -h```

## Main usage
```./is_rainy.py <city_name>```

## Example
```./is_rainy.py Paris```

2020-11-15 10:26:53,897 - city_weather - INFO - Tomorrow forecast for Paris : Light Rain

{"Paris": "Light Rain"}

## Launch tests
```pytest test.py -s```

